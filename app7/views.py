from django.shortcuts import render, redirect

def story7(request):
    return render(request, 'story7.html')

def redirecting(request):
	return redirect('/story7/')
